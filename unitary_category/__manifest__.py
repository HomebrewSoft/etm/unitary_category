# -*- coding: utf-8 -*-
{
    "name": "Unitary Category",
    "version": "13.0.0.1.0",
    "author": "HomebrewSoft",
    "website": "https://gitlab.com/HomebrewSoft/{project}/unitary_category",
    "license": "LGPL-3",
    "depends": [],
    "data": [
        # security
        # data
        # reports
        # views
        "views/unitary_category.xml",
    ],
}
