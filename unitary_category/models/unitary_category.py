# -*- coding: utf-8 -*-
from odoo import _, api, fields, models


class unitary_category(models.Model):
    _inherit = "res.partner"

    unitary_category_id = fields.Many2one(
        comodel_name="res.partner.category",
        compute="_get_category",
        store=True,
    )

    @api.depends("category_id")
    def _get_category(self):
        self.unitary_category_id = self.category_id[:1]
